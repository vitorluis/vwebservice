<?php

/**
 * Virtual SEC
 * DESENVOLVIDO POR: VITOR VILLAR
 * DESENVOLVIDO EM.: 21-06-2013
 *
 * DESCRIÇÃO: Classe responsavel por montar as querys SQL
 * 
 */
class QueryBuilder {

    //Declara algumas propriedades
    private $type = null; /* Qual é o tipo de query, se é select, insert update ou delete */
    private $data = null; /* Array de dados contendo os dados, para insert e update */
    private $fields = null;
    private $table = null;
    private $values = null; /* Utilizado para insert e update */
    private $where = null; /* Array contendo campo/valor. Pode ser usando em select, update e delete */
    private $sql = null; /* SQL gerada para enviar para a classe que executara a query */

    /*
     * Declara algumas constantes
     * Tipos de query 
     */

    const SELECT_QUERY = 1;
    const INSERT_QUERY = 2;
    const UPDATE_QUERY = 3;
    const DELETE_QUERY = 4;

    /* Operadores de comparação */
    const EQ = "eq"; //Igual
    const NE = "ne"; //Não é igual
    const LT = "lt"; //Menor que
    const GT = "gt"; //Maior que
    const LE = "le"; //Menor ou Igual
    const GE = "ge"; //Maior ou Igual
    const BW = "bw"; //Between
    const LK = "lk"; //Like

    public function getSql() {
        return $this->sql;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function setFields($fields) {
        $this->fields = $fields;
    }

    public function setValues($values) {
        $this->values = $values;
    }

    public function setWhere($where) {
        $this->where = $where;
    }

    public function setSql($sql) {
        $this->sql = $sql;
    }

    public function setTable($table) {
        $this->table = $table;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function build() {
        /*
         * Essa função retorna um boolean, que se for falso
         * Quer dizer que o build da Query não deu certo
         */
        if ($this->type != null) {
            /* Verifica qual é o tipo de query a se montar */
            switch ($this->type) {
                case self::SELECT_QUERY:
                    /* Chama a função que gera select */
                    return $this->buildSelectStatement();
                case self::INSERT_QUERY:
                    return $this->buildInsertStatement();
                case self::UPDATE_QUERY:
                    return $this->buildUpdateStatement();
                case self::DELETE_QUERY:
                    return $this->buildDeleteStatement();
                default :
                    return false;
            }
        }
    }

    private function buildSelectStatement() {
        /* Começa montar a query de select */
        $this->sql = "SELECT ";

        /*
         *  Se tiver o array com os campos, coloca eles na query, caso contrario
         *  Coloca um asterisco (*)
         */
        if (is_array($this->fields) && sizeof($this->fields) > 0) {
            /*
             * Se entrar aqui, tem alguam coisa, concatena na query $where
             * Fazendo o implode, separado por virgula
             */
            $this->sql .= implode(",", $this->fields);
        } else {
            /*
             * Concatena o asterisco para recuperar todos os campos
             */
            $this->sql .= "*";
        }
        
        

        /* Concatena agora o nome da tabela */
        $this->sql .= " FROM " . $this->table;

        /*
         * Verifica se tem where, se tiver, tem que fazer um tratamento especial
         * pois o nome do campo vem concatenado com o operador e com isso tem que
         * ser feito o desmembramento para ser colocado na query
         * 
         */
        echo $this->sql;
        
        if (is_array($this->where) && sizeof($this->where) > 0) {
            /** Concatena a keyword where na query */
            $this->sql .= " WHERE ";

            /* Chama a função que faz o tratamento e retorna o where pronto */
            $where_clause = $this->parseWhereClause($this->where);

            /* Verifica se a where clause não é null, se for, retorna false */
            if ($where_clause == null) {
                /** Seta null na sql para não haver problemas */
                $this->sql = null;

                /** E retorna o falso */
                return false;
            } else {
                /* Se não for null, então tem uma clausula where válida
                 * Concatena na sql
                 */
                $this->sql .= $where_clause;
            }
        }

        /** Se chegar aqui, deu tudo certo na construção da query, retorna true */
        return true;
    }

    private function buildInsertStatement() {
        
    }

    private function buildUpdateStatement() {
        
    }

    private function buildDeleteStatement() {
        
    }

    private function parseWhereClause($where) {
        /** Dclara algumas vars */
        $where_clauses = array();
        $where = null;

        /** Faz o loop passando pelo array para pegar o valor e chave */
        foreach ($where as $key => $value) {
            /** O valor do $value não interessa, então faz o unset */
            unset($value);

            /** Verifica se o parametro tem o operador */
            if (strpos($key, "_") != false) {
                /** Faz o explode separando campo e operador */
                $split = explode("_", (array) $key);
                $field = $split[0];
                $operator = $split[1];

                /** Limpa o slipt */
                unset($split);

                /** Concatena o campo na variavel de retorno */
                $where = $field;

                /** Limpa o field */
                unset($field);

                /** Agora valida e concatena o operador */
                switch ($operator) {
                    case self::EQ:
                        $where .= " = ?";
                        break;
                    case self::NE:
                        $where .= " <> ?";
                        break;
                    case self::LT:
                        $where .= " < ?";
                        break;
                    case self::GT:
                        $where .= " > ?";
                        break;
                    case self::LE:
                        $where .= " <= ?";
                        break;
                    case self::GE:
                        $where .= " >= ?";
                        break;
                    case self::BW:
                        $where .= " between ? and ? ";
                        break;
                    case self::LK:
                        $where .= " like ?";
                        break;
                }
            } else {
                $where = null;
                return null;
            }

            /** Adiciona a where clause no array de clausulas */
            if ($where != null)
                array_push($where_clauses, $where);
        }

        /** Retorna o where clause */
        return implode(" AND ", $where_clauses);
    }

}
