<?php

/**
 * Virtual SEC
 * DESENVOLVIDO POR: VITOR VILLAR
 * DESENVOLVIDO EM.: 14-06-2013
 *
 * DESCRIÇÃO: Classe que guarda configurações do banco de dados
 * 
 */

class DbConnection extends PDO {
    
    //Propriedades da classe de conexão
    private $str_conn;
    
    public function __construct() {
        $this->str_conn = DbConfig::DB_DRIVER . ":host=" . DbConfig::DB_HOST;
        $this->str_conn .= ";port=" . DbConfig::DB_PORT;
        
        //Executa o construtor do PDO
        parent::__construct($this->str_conn, DbConfig::DB_USER, DbConfig::DB_PASSWD);
    }
}
