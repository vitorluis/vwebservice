<?php

/**
 * Virtual SEC
 * DESENVOLVIDO POR: VITOR VILLAR
 * DESENVOLVIDO EM.: 20-06-2013
 *
 * DESCRIÇÃO: Arquivo que faz os includes de todos os arquivos necessários
 * 
 */
require_once("config/DbConfig.php");
require_once("config/HttpHeaders.php");
require_once("config/Validation.php");
require_once("config/Security.php");
require_once("db/DbConnection.php");
require_once("db/DbStatement.php");
require_once("db/QueryBuilder.php");
require_once("method/Method.php");
require_once("method/Get.php");
require_once("method/Post.php");
require_once("method/Put.php");
require_once("method/Delete.php");
require_once("requests/Request.php");
require_once("requests/Response.php");