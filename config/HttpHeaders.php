<?php

/**
 * Virtual SEC
 * DESENVOLVIDO POR: VITOR VILLAR
 * DESENVOLVIDO EM.: 13-06-2013
 *
 * DESCRIÇÃO: Classe que é respnsavel pelos headers http
 * para a requisição
 * 
 */
class HttpHeaders {

    //A propriedade header conterá os headers a serem enviados no momento da resposta
    //Da requisição solicitada
    private $header = array();

    //Http Codes
    const HTTP_1_1 = "HTTP/1.1";
    const HTTP_200_CODE = "200 OK";
    const HTTP_204_CODE = "204 No Content";
    const HTTP_400_CODE = "400 Bad Request";
    const HTTP_401_CODE = "401 Unauthorized";
    const HTTP_405_CODE = "405 Method Not Allowed";
    
    //Other Headers
    const HTTP_CACHE_CONTROL = "Cache-Control: no-cache";
    const HTTP_CONTENT_TYPE_JSON = "Content-Type: application/json";
    const HTTP_CONTENT_TYPE_XML = "Content-Type: application/xml";
    const HTTP_ACCESS_CONTROL = "Access-Control-Allow-Origin: *";

    public function sendHeaders() {
        foreach ($this->header as $header) {
            header($header);
        }
    }

    public function setHttpCode($status_code) {
        $this->header[] = self::HTTP_1_1 . " " . $status_code;
    }

    public function addHeader($header) {
        $this->header[] = $header;
    }

}
