<?php

/**
 * Virtual SEC
 * DESENVOLVIDO POR: VITOR VILLAR
 * DESENVOLVIDO EM.: 13-06-2013
 *
 * DESCRIÇÃO: Classe que guarda configurações do banco de dados
 * 
 */

class DbConfig {
    
    //Configuração do banco
    const DB_HOST   = "localhost";
    const DB_PORT   = 5432;
    const DB_USER   = "postgres";
    const DB_PASSWD = "postgres";
    
    /*
     * Possiveis driver:
     * pgsql  - PostgreSQL
     * mysql  - MySQL
     * oci    - Oracle
     * sqlite - SQLite
     */
    const DB_DRIVER = "pgsql";
    const DB_NAME   = "vfirewall";
}