<?php
/**
 * Virtual SEC
 * DESENVOLVIDO POR: VITOR VILLAR
 * DESENVOLVIDO EM.: 13-06-2013
 *
 * DESCRIÇÃO: Arquivo que começa o processo da requisição
 * 
 */
error_reporting(E_ALL);
require_once("config/Includes.php");
$request = new Request();
$request->setHeaders( apache_request_headers() );
$request->setParams( $_GET );
$request->processRequest();
$request->getResponse();



?>