<?php

/**
 * Virtual SEC
 * DESENVOLVIDO POR: VITOR VILLAR
 * DESENVOLVIDO EM.: 14-06-2013
 *
 * DESCRIÇÃO: Classe recebe os dados e headers e processa a requisição
 * 
 */

class Request {
   
    //Seta as propriedades
    private $headers;
    private $data;
    private $params;
    
    //Objeto que guarda a resposta
    private $response;
    
    public function setHeaders($headers) {
        $this->headers = $headers;
    }
    
    public function setParams($params) {
        $this->params = $params;
    }

    public function setData($data) {
        $this->data = $data;
    }
    
    private function setResponse($response) {
        $this->response = $response;
    }
    
    public function getResponse() {
        return $this->response;
    }
    
    public function processRequest() {
        
        $method = new Method();
        $method->setData($this->data);
        $method->setHeaders($this->headers);
        $method->setParams( $this->params );
        
        $method->processRequest();
    }
    
    

}
