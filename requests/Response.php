<?php
/**
 * Virtual SEC
 * DESENVOLVIDO POR: VITOR VILLAR
 * DESENVOLVIDO EM.: 13-06-2013
 *
 * DESCRIÇÃO: Classe responsavel pela resposta das requisições
 * para a requisição
 * 
 */

use HttpHeaders;

class Response {

    //Propriedades da classe de response
    private $data;
    private $output_format;
    private $http_header;
    private $encoded_data;
    
    //Declara algumas constantes
    const JSON_FORMAT = 1;
    const XML_FORMAT = 2;

    public function getRawData() {
        return $this->data;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setOutputFormat($output_format) {
        $this->output_format = $output_format;
    }

    public function setHttpHeader($http_header) {
        $this->http_header = $http_header;
    }

    public function sendHeaders() {
        //Manda todos os headers
        if ($this->http_header instanceof HttpHeaders) {
            $this->http_header->sendHeaders();
        }
    }
    
    public function getEncodedData() {
        if ($this->output_format == self::JSON_FORMAT) {
            $this->encoded_data = json_encode($this->data);
        } else if ($this->output_format == self::XML_FORMAT) {
            //Do nothing yet.
        }
        return $this->encoded_data;
    }

}
