<?php

/**
 * Virtual SEC
 * DESENVOLVIDO POR: VITOR VILLAR
 * DESENVOLVIDO EM.: 14-06-2013
 *
 * DESCRIÇÃO: Classe que verifica qual é o metodo para ser executado
 * 
 */
class Method {

    //Propriedades que todas as classes
    private $data;
    private $header;
    private $params;
    private $response;
    private $method = null;

    public function setData($data) {
        $this->data = $data;
    }

    public function setHeaders($header) {
        $this->header = $header;
    }

    public function setParams($params) {
        $this->params = $params;
    }

    private function verifyMethod() {
        //Guarda numa var qual é o metodo
        $method = filter_input(INPUT_SERVER, "REQUEST_METHOD");

        //Verifica agora os valores
        switch ($method) {
            case "GET":
                //Instancia uma classe de get
                $this->method = new Get();
                break;
            case "POST":
                //Instancia uma classe POST
                $this->method = new Post();
                break;
            case "PUT":
                //Instancia uma classe de PUT
                $this->method = new Put();
                break;
            case "DELETE":
                //Instancia a classe de delete
                $this->method = new Delete();
                break;
            default:
                $this->method = null;
                break;
        }
    }

    public function processRequest() {
        //Verifica qual é o metodo selecionado
        $this->verifyMethod();

        //Verifica se a propriedade metodo é null ou uma classe para processar 
        //A requisição

        if ($this->method == null) {
            //Se o method for null, quer dizer que não é um método válido
            //Então instancia um Objeto do tipo response para enviar o codigo
            //HTTP e a mensagem de erro
            $this->response = new Response();
        } else {
            //Passa os headers, parametros e data
            $this->method->setParams($this->params);
            $this->method->setHeaders($this->header);
            $this->method->setData($this->data);

            //Faz o processamento
            $this->method->process();

            //Guarda a resposta
            $this->response = $this->method->getResponse();
        }
    }

    public function getResponse() {
        return $this->response;
    }

}
