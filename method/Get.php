<?php

/**
 * Virtual SEC
 * DESENVOLVIDO POR: VITOR VILLAR
 * DESENVOLVIDO EM.: 18-06-2013
 *
 * DESCRIÇÃO: Classe para processar requisições via GET
 * 
 */
class Get {

    //Delcara os metodos
    private $headers;
    private $data;
    private $params;
    private $response;
    private $table;
    private $fields;
    private $where;

    //Metodo para fazer o parse da query string
    private function parseParams() {
        /*
         * Os parametros vem como um array da variavel global $_GET
         * Então faz um loop para pegar os valores e setar nas propriedades
         * Essa função retorna um parametro boolean informando se os parametros
         * Foram parseados com sucesso, pelo menos os campos obrigatório
         */

        //Verifica se tem o parametro "table"
        if (!is_array($this->params) || !array_key_exists("table", $this->params))
        //Não existe o parametro table, ou não é um array
            return false;
        else
            $this->table = $this->params["table"];

        //Verifica se tem o parametros fields, que indica quais os campos devem
        //Ser recuperados pelo get
        if (array_key_exists("fields", $this->params))
            $this->fields = $this->params["fields"];


        //Se chegar aqui, faz um loop nos parametros, validando as chaves/valores
        foreach ($this->params as $param => $value) {
            
            //Faz a validação
            if ($this->validateParam($param, $value)) {
                //Se retornar true, tanto o parametro como o valor são validos
                //E não tem nenhum tipo de coisa errada que possa interferir 
                //No processamento
                $this->where[$param] = $value;
            } else {
                //Se entrar aqui alguma vez, retorna falso, pois não é uma
                //requisição válida
                return false;
            }
        }
        
        //Se chegar aqui, tudo OK, retorna true, para continuar com o processamento
        return true;
    }

    //Metodo para fazer validacoes dos parametros
    private function validateParam($param, $value) {
        //Aplica os filtros de validação do param/valor
        //Começando pelo parametro
        return true;
    }

    //Metodo para gerar o sql
    private function generateSQL() {
        
    }

    //Metodo para processar e pegar resposta
    public function process() {
        //Começa fazendo o parse dos parametros
        $this->parseParams();

        /** Chama o queryBuilder */
        $query = new QueryBuilder();
        $query->setData( $this->data );
        $query->setFields( $this->fields );
        $query->setTable( $this->table );
        $query->setWhere( $this->where );
        $query->setType( QueryBuilder::SELECT_QUERY);
        $query->build();
        //echo $query->getSql();
    }

    public function setHeaders($headers) {
        $this->headers = $headers;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setParams($params) {
        $this->params = $params;
    }

    public function getResponse() {
        return $this->response;
    }

}
