<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Post
 *
 * @author vitor
 */
class Post {
    
    //Delcara os metodos
    private $headers;
    private $data;
    private $params;
    private $response;

    //Metodo para fazer o parse da query string
    private function parseParams() {
        
    }
    
    //Metodo para fazer validacoes dos parametros
    private function validateParams() {
        
    }
    
    //Metodo para gerar o sql
    private function generateSQL() {
        
    }
    
    //Metodo para processar e pegar resposta
    public function process() {
        
    }
    public function setHeaders($headers) {
        $this->headers = $headers;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setParams($params) {
        $this->params = $params;
    }

    public function getResponse() {
        return $this->response;
    }
}
